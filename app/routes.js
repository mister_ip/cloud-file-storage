'use strict';

var express     = require('express');
var fs          = require('fs');
var router      = express.Router();

var handlers    = {};
var handlersDir = './handlers/';

// Bootstrap handlers
// fs.readdirSync(handlersDir).forEach(function (file) {
//   if (~file.indexOf('.js')) {
//     handlers[file.replace('.js', '')] = require(handlersDir + file);
//   }
// });

// Routes
// router.get('/users', handlers.users.list);
// router.post('/users', handlers.users.create);
// router.get('/users/:id', handlers.users.get);
// router.patch('/users/:id', handlers.users.update);
// router.delete('/users/:id', handlers.users.remove);

router.get('/users', function(req, res, next){res.json({message: "pong"})});

module.exports = router;