var convict = require('convict');

var conf = convict({
  env: {
    doc: 'The applicaton environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'STORAGE_ENV',
    arg: 'env'
  },
  version: {
    doc: 'The applicaton version.',
    format: String,
    default: 'v1',
    env: 'STORAGE_VERSION'
  },
  server: {
    ip: {
      doc: 'The IP address to bind.',
      format: 'ipaddress',
      default: '0.0.0.0',
      env: 'STORAGE_SERVER_IP_ADDRESS',
      arg: 'ip'
    },
    port: {
      doc: 'The port to bind.',
      format: 'port',
      default: 3000,
      env: 'STORAGE_SERVER_PORT',
      arg: 'port'
    }
  },
  database: {
    url: {
      doc: 'Database connect url.',
      format: String,
      default: 'mongodb://localhost:27017/test',
      env: 'STORAGE_DATABASE_URL'
    },
    options: {
      doc: 'Mongodb options.',
      format: Object,
      default: {}
    }
  },
  logger: {
    name: {
      doc: 'All loggers must provide a `name`',
      format: String,
      default: 'file-storage-app'
    },
    level: {
      doc: 'The log levels in bunyan',
      format: ['fatal', 'error', 'warn', 'info', 'debug', 'trace'],
      default: 'error',
      env: 'STORAGE_LOG_LEVEL'
    }
  }
});

// Load environment dependent configuration
var env = conf.get('env');
conf.loadFile(['./config/' + env + '.json', './config/config.json']);

// Perform validation
conf.validate({strict: true});

module.exports = conf;
