'use strict';

var util = require('util');

module.exports = HttpError;

function HttpError(message, status) {
    this.message = message;
    this.status = status || 400;
}

util.inherits(HttpError, Error);

HttpError.prototype.name = 'HttpError';