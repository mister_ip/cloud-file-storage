var bunyan = require('bunyan');
var config = require('./config');

module.exports = bunyan.createLogger({
    name: config.get('logger.name'),
    stream: process.stdout,
    level: config.get('logger.level'),
    serializers: {
      req: reqSerializer
    }
});

function reqSerializer(req) {
  return {
    method: req.method,
    url: req.url
  }
}