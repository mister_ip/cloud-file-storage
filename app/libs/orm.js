'use strict';

var uuid = require('node-uuid');

module.exports = {
  init: init,
  model: model
};

var _db,
    _validator,
    _modelsDir,
    _models;

function init(dbConnection, validator, modelsDir) {
  _db = dbConnection;
  _validator = validator;
  _modelsDir = modelsDir;
}

function lodadModels(modelsDir) {
  fs.readdirSync(modelsDir).forEach(function (file) {
    if (~file.indexOf('.js')) {
      require(path.join(modelsDir, file));
    }
  });
}

function model(name, schema) {
  if(schema) {
    return setModel(name, schema);
  }

  return getModel(name);
}

function setModel(name, schema) {
  if(_models[name]) {
    throw new Error(['This model `',name,'` is already registered'].join(''));
  }
  return _models[name] = new Model(name, schema);
}

function getModel(name) {
  if(_models[name]) {
    return _models[name];
  }
  throw new Error(['This model `',name,'` does not exists'].join(''));
}

function Model(name, scheme) {
  this.name = name;
  this.scheme = scheme;
  this.fields = Object.keys(scheme)
      .reduce(function(res, current){
        res[current] = 1;
        return res;
      }, {});

  this.fields._id = 0;
  this.fields.id = "$_id";

  validator.registerModel(name, scheme);
}

Model.prototype.getAll = function(cb) {
  db.getConnect()
    .collection(this.name)
    .aggregate([
        { $match: {deleted: {$ne: true}} },
        { $project: this.fields }
    ])
    .toArray(function(err, docs) {
      if(err)
        cb(err);
      else
        cb(null, docs);
    });
};

Model.prototype.getById = function(id, cb) {
  db.getConnect()
    .collection(this.name)
    .aggregate([
      { $match: {_id: id, deleted: {$ne: true}} },
      { $project: this.fields }
    ])
    .toArray(function(err, docs) {
      if(err)
        cb(err);
      else
        if(docs.length)
          cb(null, docs[0]);
        else
          cb(null, docs[0]);
    });
};

Model.prototype.create = function(obj, cb) {
  if(!obj.id) {
    obj.id = uuid.v4();
  }

  if(validator.validate(this.name, obj)) {
    obj._id = obj.id;
    delete obj.id;

    db.getConnect()
      .collection(this.name)
      .insertOne(obj, function(err) {
        if(err)
          cb(err);
        else {
          obj.id = obj._id;
          delete obj._id;
          cb(null, obj);
        }
      });
  }
  else {
    cb(new httpError('Validation error', 400));
  }
};

Model.prototype.updateById = function(id, obj, cb) {
  if(!validator.validate(this.name, obj)) {
    return cb(new httpError('Validation error', 400));
  }

  delete obj.id;

  db.getConnect()
    .collection(this.name)
    .updateOne({$and: [{_id: id}, {deleted: {$ne: true}}]}, obj, function(err, res) {
      if(err) {
        return cb(err);
      }

      cb(null, res.result.nModified === 1);
    });
};

Model.prototype.removeById = function(id, cb) {
  db.getConnect()
    .collection(this.name)
    .updateOne({_id: id}, {$set: {deleted: true}}, function(err, res) {
      if(err)
        cb(err);
      else
        cb(null, res.result.nModified === 1);
    });
};