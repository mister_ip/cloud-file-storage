'use strict';

var tv4  = require('tv4');
var fs   = require('fs');
var path = require('path');

tv4.addFormat('uuid', function (data, schema) {
  var reg = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$', 'i');

  if (typeof data === 'string' && reg.test(data)) {
      return null;
  }
  return 'must be string of uuid v4';
});

tv4.loadSchems = function(schemasDir, cb) {
  try {
    fs.readdirSync(schemasDir).forEach(function (file) {
      if (~file.indexOf('.schema')) {
        var schema = JSON.parse(fs.readFileSync(path.join(schemasDir, file)));
        var name = path.parse(file).name;

        tv4.addSchema(name, schema);
      }
    });
    cb(null);
  }
  catch (err) {
    cb(err);
  }
};

module.exports = tv4;
