'use strict';

var express    = require('express');
var bodyParser = require('body-parser');
var httpError  = require('./httpError');
var logger     = require('./logger').child({module: 'http-server'});

module.exports = {
  start: start,
  stop: stop,
  restart: restart
};

var app = express();
var server;

app.use(expressLogger);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function start(options, cb) {
  if(typeof cb !== 'function') cb = new Function();

  //if flag isDisconnected is true
  app.use(function(req, res, next) {
    if(GLOBAL.isDisconnected) {
      return next(new httpError('Databese is disconnected', 503));
    }

    next();
  });

  // set routes
  app.use('/'+options.prefix, options.router);

  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    next(new httpError('Not Found', 404));
  });

  // error handler
  app.use(expressErrors);

  server = app.listen(options.port, options.host, function (err) {
    if(err) {
      logger.error(err);
      return cb(err);
    }

    logger.info('server is running on port ' + options.port);
    cb(null, server);
  });
}

function stop(cb) {
  if(typeof cb !== 'function') cb = new Function();

  if(server) {
    server.close(cb);
    logger.info('server is stopped');
  }
  else {
    setImmediate(cb, new Error('Http server not running'));
  }
}

function restart(options, cb) {
  if(typeof cb !== 'function') cb = new Function();

  stop(function(err){
    if(err) {
      return cb(err);
    }

    start(options, cb);
  })
}

function expressLogger(req, res, next) {
  logger.info({req: req});
  next();
}

function expressErrors(err, req, res, next) {
  res.status(err.status || 500);

  if(err.message) {
    res.json({error: err.message});
  }

  res.end();

  if(err.status < 500) {
    logger.warn(err);
  }
  else {
    logger.error(err);
  }
};
