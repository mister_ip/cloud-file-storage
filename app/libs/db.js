'use strict';

var MongoClient = require('mongodb').MongoClient;
var logger      = require('./logger').child({module: 'db'});

module.exports = {
  connect: connect,
  getConnection: getConnection,
  getCollection: getCollection,
  close: close
};

var connection;
var collections = {};
var errorMessages = ['The database is not initialized or connection is closed'];

function connect(url, options, cb) {
  if(typeof cb !== 'function') cb = new Function();

  MongoClient.connect(url, options, function(err, db) {
    if(err) {
      logger.error(err);
      return cb(err);
    }

    db.on('close', function() {
      GLOBAL.isDisconnected = true;
    });

    db.on('reconnect', function() {
      GLOBAL.isDisconnected = false;
    });

    db.on('error', function(err) {
      logger.error(err);
    });

    connection = db;
    cb(null, db);
    logger.info('connected correctly to server `' + url + '`');
  });
}

function getConnection() {
  if(connection) {
    return connection;
  }
  throw new Error(errorMessages[0]);
}

function getCollection(name) {
  if(connection) {
    return collections[name] ? collections[name] : collections[name] = connection.collection(name);
  }
  throw new Error(errorMessages[0]);
}

function close(cb) {
  if(typeof cb !== 'function') cb = new Function();

  if(connection) {
    return connection.close(false, cb);
  }
  setImmediate(cb,new Error(errorMessages[0]));
}