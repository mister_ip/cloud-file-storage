'use strict';

var should  = require('should');
var request = require('supertest');
var _       = require('lodash');
var fs      = require('fs');

describe('Api http rest tests', function () {
  var port, version, validator, database;

  before(function(done) {
    process.env.STORAGE_ENV = 'test';

    require('../app').once('ready', function(app) {
      port      = app.config.get('server.port');
      version   = app.config.get('version');
      validator = app.validator;
      database  = app.database;
      done();
    });
  });

  describe('users routes', function () {
    var newUser = JSON.parse(fs.readFileSync('./examples/user-new.json'));
    var usersList = JSON.parse(fs.readFileSync('./examples/users.json'));
    var retrieveUser = JSON.parse(fs.readFileSync('./examples/user-retrieve.json'));

    beforeEach(function () {
      this.request = request(['http://localhost:',port,'/',version,'/users'].join(''));
    });

    afterEach(function () {
      this.request = null;
    });

    describe('GET users/ request', function () {
      before(function(done) {

      });

      it('should return list of users', function (done) {
        this.request.get('/')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
              if (err) {
                return done(err);
              }

              //checkErrorResponse(res, /^pong/);

              var data = {
                "id": "5354d246-cb00-420d-be47-c23695fd690a",
                "name": {
                  "first": "John",
                  "last": "Doe"
                },
                "email": "john@mail.com",
                "metadata": {
                  "phone": "+380349823234"
                }
              }


              if(!validator.validate(data, 'user')) {
                throw new Error(validator.error.message)
              };

              done(null);
            });
      });

      after(function(done) {

      });
    });
  });

});

function checkErrorResponse(response, regexError) {
  response.body.should.be.instanceof(Object);
  response.body.message.should.match(regexError);
}