'use strict';

var async        = require('neo-async');
var config       = require('./app/libs/config');
var httpServer   = require('./app/libs/http-server');
var router       = require('./app/routes');
var database     = require('./app/libs/db');
var logger       = require('./app/libs/logger');
var validator    = require('./app/libs/validator');
var events       = require('events');
var fs           = require('fs');
var path         = require('path');
var eventEmitter = new events.EventEmitter();
var modelsDir    = './app/models';

module.exports = eventEmitter;

async.series([
  function(cb){
    // init database
    database.connect(
      config.get('database.url'),
      config.get('database.options'), 
    cb);
  },
  function(cb){
    // load schemas
    validator.loadSchems('./schemas', cb);
  },
  function(cb){
    // load models
    fs.readdirSync(modelsDir).forEach(function (file) {
      if (~file.indexOf('.js')) {
        require(path.join(modelsDir, file));
      }
    });

    // run http server
    httpServer.start({
      host: config.get('server.ip'),
      port: config.get('server.port'),
      prefix: config.get('version'),
      router: router
    }, cb);
  }
],
function(err){
  if (err) {
    process.exit(1);
  }

  process.on('SIGTERM', function () {
    async.series([httpServer.stop, database.close], function(){
      process.exit(0);
    });
  });

  eventEmitter.emit('ready', {
    config: config,
    database: database,
    validator: validator,
    httpServer: httpServer
  });

  logger.info('server is ready');
});

process.on('uncaughtException', function(err) {
  logger.fatal(err);
});
